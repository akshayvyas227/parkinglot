package swiggy.bootcamp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValetTest {

    private static ParkingLot getFullParkingLot(List<ParkingLotNotifiable> parkingLotNotifiable) throws ParkingLotException {
        ParkingLot parkingLot = new ParkingLot(1, parkingLotNotifiable, new HashSet<>());
        parkingLot.park(new Vehicle());
        return parkingLot;
    }

    @Test
    @DisplayName("When ParkingLot is available")
    void valetTestWhenParkingLotIsAvailable() {
        ParkingLot parkingLot = new ParkingLot(1, new ArrayList<>(), new HashSet<>());
        ParkingLot fullParkingLot = new ParkingLot(1, new ArrayList<>(), new HashSet<>());
        List<ParkingLot> parkingLots = new ArrayList<>();

        parkingLots.add(fullParkingLot);
        parkingLots.add(parkingLot);
        Valet valet = new Valet(parkingLots);
        ParkingStyle parkingStyle = ParkingStyle.DEFAULT;

        assertDoesNotThrow(() -> valet.park(new Vehicle(),parkingStyle));
    }
    @Test
    @DisplayName("When ParkingLot is full")
    void valetTestWhenParkingLotIsFull() throws ParkingLotException {
        ParkingLot parkingLot = getFullParkingLot(new ArrayList<>());
        ParkingLot fullParkingLot = getFullParkingLot(new ArrayList<>());
        List<ParkingLot> parkingLots = new ArrayList<>();

        parkingLots.add(fullParkingLot);
        parkingLots.add(parkingLot);
        Valet valet = new Valet(parkingLots);
        ParkingStyle parkingStyle = ParkingStyle.DEFAULT;

       assertThrows(ParkingLotException.class, () -> valet.park(new Vehicle(), parkingStyle));
    }

    @Test
    @DisplayName("Expect parkingLot is identified")
    void expectParkingLotNameWhenSearchingForAVehicle() throws ParkingLotException {
        ParkingLot parkingLot = new ParkingLot(1, new ArrayList<>(), new HashSet<>());
        ParkingLot otherParkingLot = new ParkingLot(1, new ArrayList<>(), new HashSet<>());
        List<ParkingLot> parkingLots = new ArrayList<>();

        parkingLots.add(parkingLot);
        parkingLots.add(otherParkingLot);
        Valet valet = new Valet(parkingLots);
        Vehicle car = new Vehicle();
        ParkingStyle parkingStyle = ParkingStyle.DEFAULT;
        valet.park(car, parkingStyle);

        assertEquals(parkingLot, valet.whichParkingLot(car));
    }

    @Test
    @DisplayName("Expect parking is not identified correctly")
    void doesNotExpectOtherParkingLotNameWhenSearchingForAVehical() throws ParkingLotException {
        ParkingLot parkingLot = new ParkingLot(2);
        ParkingLot otherParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();

        parkingLots.add(parkingLot);
        parkingLots.add(otherParkingLot);
        Valet valet = new Valet(parkingLots, true);
        Vehicle car = new Vehicle();
        ParkingStyle parkingStyle = ParkingStyle.DEFAULT;
        valet.park(car, parkingStyle);

        assertNotEquals(otherParkingLot, valet.whichParkingLot(car));
    }


    @Test
    @DisplayName("Expect exception to be thrown when no parkingLot contains that car")
    void expectExceptionToBeThrownWhenCarIsNotParkedInAnyParkingLot() {
        ParkingLot parkingLot = new ParkingLot(1, new ArrayList<>(), new HashSet<>());
        ParkingLot otherParkingLot = new ParkingLot(1, new ArrayList<>(), new HashSet<>());
        List<ParkingLot> parkingLots = new ArrayList<>();

        parkingLots.add(parkingLot);
        parkingLots.add(otherParkingLot);
        Valet valet = new Valet(parkingLots);

        assertThrows(ParkingLotException.class, () -> valet.whichParkingLot(new Vehicle()));
    }


    @Test
    @DisplayName("Expect parkingLot 2 to be assigned")
    void expectMinimumOccupiedParkingLotToBeAssigned() throws ParkingLotException {
        ParkingLot firstParkingLot = new ParkingLot(3);
        ParkingLot secondParkingLot = new ParkingLot(3);

        firstParkingLot.park(new Vehicle());
        firstParkingLot.park(new Vehicle());
        secondParkingLot.park(new Vehicle());
        Valet valet = new Valet(Arrays.asList(firstParkingLot, secondParkingLot), true);
        Vehicle car = new Vehicle();
        ParkingStyle parkingStyle = ParkingStyle.LOT_WITH_LEAST_VEHICLES;
        valet.park(car,parkingStyle);

        assertTrue(secondParkingLot.isParked(car));
    }

    @Test
    @DisplayName("Expect parkingLot 1 maximum free space to be assigned")
    void expectDefaultParkingLotToBeAssigned() throws ParkingLotException {
        ParkingLot firstParkingLot = new ParkingLot(10);
        ParkingLot secondParkingLot = new ParkingLot(3);

        firstParkingLot.park(new Vehicle());
        firstParkingLot.park(new Vehicle());
        secondParkingLot.park(new Vehicle());
        Valet valet = new Valet(Arrays.asList(firstParkingLot, secondParkingLot), true);
        Vehicle car = new Vehicle();
        ParkingStyle parkingStyle = ParkingStyle.DEFAULT;
        valet.park(car,parkingStyle);

        assertTrue(firstParkingLot.isParked(car));
    }

    @Test
    @DisplayName("Expect parkingLot 1 to be assigned")
    void expectMaximumFreeSpaceParkingLotToBeAssigned() throws ParkingLotException {
        ParkingLot firstParkingLot = new ParkingLot(10);
        ParkingLot secondParkingLot = new ParkingLot(3);

        firstParkingLot.park(new Vehicle());
        firstParkingLot.park(new Vehicle());
        firstParkingLot.park(new Vehicle());
        firstParkingLot.park(new Vehicle());
        firstParkingLot.park(new Vehicle());
        secondParkingLot.park(new Vehicle());
        Valet valet = new Valet(Arrays.asList(firstParkingLot, secondParkingLot), true);
        Vehicle car = new Vehicle();
        ParkingStyle parkingStyle = ParkingStyle.LOT_WITH_MOST_SPACES;
        valet.park(car,parkingStyle);

        assertTrue(firstParkingLot.isParked(car));
    }
}