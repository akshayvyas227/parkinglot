package swiggy.bootcamp;

class OwnerTest implements ParkingLotNotifiable {
    boolean isMyParkingLotFull = false;

    public void notifyIsFull(){
        isMyParkingLotFull = true;
    }

    public void notifySpaceIsAvailable() {

    }
}
