package swiggy.bootcamp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ParkingLotTest {

    private final ParkingLotNotifiable ownerTest = mock(ParkingLotNotifiable.class);

    @DisplayName("Accept a car when Parking lot size is 1")
    @Test
    void expectTrueWhenOneCarArrives() {
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(1, owners);

        assertDoesNotThrow(() -> parkingLot.park(aVehicle()));
    }

    private static Vehicle aVehicle() {
        return new Vehicle();
    }

    @DisplayName("Do not accept a car when Parking lot size is 0")
    @Test
    void expectFalseWhenOneCarArrivesToParkingLotOfZeroCapacity() {
        ParkingLot parkingLot = new ParkingLot(-1, new ArrayList<>());

        assertThrows(ParkingLotException.class, () -> parkingLot.park(aVehicle()));
    }

    @DisplayName("Do not allow car to be parked twice")
    @Test
    void expectFalseWhenCarAlreadyParked() throws ParkingLotException {
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        Vehicle vehicle = aVehicle();
        parkingLot.park(vehicle);

        assertThrows(ParkingLotException.class, () -> parkingLot.park(vehicle));
    }

    @DisplayName("Allow UnPark for car when place is available")
    @Test
    void expectTrueWhenOneCarIsUnParkedInLotOfTwoCapacity() throws ParkingLotException {
        Vehicle vehicle = aVehicle();
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        parkingLot.park(vehicle);

        assertDoesNotThrow(() -> parkingLot.unPark(vehicle));
    }

    @DisplayName("Do not UnPark a car when Parking lot size is 0")
    @Test
    void expectFalseWhenOneCarIsUnParked() {
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(0, owners);

        assertThrows(ParkingLotException.class, () -> parkingLot.unPark(aVehicle()));
    }

    @DisplayName("Allow UnPark for car which is present to be unParked correctly")
    @Test
    void expectTrueWhenCarPresentInTheLotIsUnParked() throws ParkingLotException {
        Vehicle redCar = aVehicle();
        Vehicle blackCar = aVehicle();
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        parkingLot.park(redCar);
        parkingLot.park(blackCar);

        assertDoesNotThrow(() -> parkingLot.unPark(redCar));
    }


    @DisplayName("Do not allow car which is not present to be unParked")
    @Test
    void expectFalseWhenCarNotPresentInTheLotIsUnParked() throws ParkingLotException {
        Vehicle blackCar = aVehicle();
        Vehicle redCar = aVehicle();
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        parkingLot.park(blackCar);

        assertThrows(ParkingLotException.class, () -> parkingLot.unPark(redCar));
    }

    @DisplayName("Expect True when checking for a car is present in the Lot")
    @Test
    void expectTrueForCheckIfParkedWhenCarIsParked() throws ParkingLotException {
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(1, owners);
        Vehicle vehicle = aVehicle();
        parkingLot.park(vehicle);

        assertTrue(parkingLot.isParked(vehicle));
    }


    @DisplayName("Expect False when checking for a car not present in the Lot")
    @Test
    void expectFalseForCheckIfParkedWhenCarIsNotParked() throws ParkingLotException {
        Vehicle blackCar = aVehicle();
        Vehicle redCar = aVehicle();
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        parkingLot.park(redCar);

        assertFalse(parkingLot.isParked(blackCar));
    }

    //Notification Test
    @Test
    void expectTrueWhenParkingLotIsFull() throws ParkingLotException {
        OwnerTest ownerTest = new OwnerTest();
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        owners.add(ownerTest);
        ParkingLot parkingLot = new ParkingLot(1, owners);
        parkingLot.park(new Vehicle());

        assertTrue(ownerTest.isMyParkingLotFull);
    }

    @Test
    void expectFalseWhenParkingLotIsNotFull() throws ParkingLotException {
        OwnerTest ownerTest = new OwnerTest();
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        parkingLot.park(aVehicle());

        assertFalse(ownerTest.isMyParkingLotFull);
    }

    @Test
    void expectFalseWhenParkingLotIsNotFullMockNotifiable() throws ParkingLotException {
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot parkingLot = new ParkingLot(2, owners);
        parkingLot.park(aVehicle());

        verify(ownerTest, never()).notifyIsFull();
    }

    @Test
    void doNotExpectLotEmptyNotificationWhenParkingLotIsNotEmptyMockNotifiable() throws ParkingLotException {
        OwnerTest owner = new OwnerTest();
        List<ParkingLotNotifiable> owners = new ArrayList<>();

        owners.add(owner);
        ParkingLot fullParkingLot = new ParkingLot(2, owners, new HashSet<>());
        fullParkingLot.park(aVehicle());

        verify(ownerTest, never()).notifySpaceIsAvailable();
    }
}
