package swiggy.bootcamp;

class Ticket {
    private int statusCode;
    Ticket(int statusCode) {
        this.statusCode = statusCode;
        try {
            process();
        }
        catch (Exception e) {

        }
    }

    private void process() throws TicketException {
        if (statusCode > 0) {
            return;
        }
        if (statusCode == 0) {
            throw new TicketException("Parking Lot is full");
        }
        throw new TicketException("Can't unPark for empty Lot");
    }
}

class TicketException extends Exception {
    TicketException(String message) {
        super(message);
    }
}
