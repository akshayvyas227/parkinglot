package swiggy.bootcamp;

// This interface enables you to observe Parking Lot Events
interface ParkingLotNotifiable {

    void notifyIsFull();

    void notifySpaceIsAvailable();
}
