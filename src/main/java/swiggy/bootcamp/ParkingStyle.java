package swiggy.bootcamp;

public enum ParkingStyle {

    LOT_WITH_MOST_SPACES, LOT_WITH_LEAST_VEHICLES, DEFAULT
}
