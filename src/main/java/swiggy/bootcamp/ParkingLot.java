package swiggy.bootcamp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

// An area to safely contain vehicles temporarily
class ParkingLot {
    private int size;
    private HashSet<Vehicle> parkedVehicles;
    private List<ParkingLotNotifiable> parkingLotNotifiable;

    ParkingLot(int size) {
        this(size, new ArrayList<>());
    }

    ParkingLot(int size, List<ParkingLotNotifiable> parkingLotNotifiable) {
        this(size, parkingLotNotifiable, new HashSet<>());
    }

    ParkingLot(int size, List<ParkingLotNotifiable> parkingLotNotifiable, HashSet<Vehicle> parkedVehicles) {
        this.size = size;
        this.parkingLotNotifiable = parkingLotNotifiable;
        this.parkedVehicles = parkedVehicles;
    }

    void park(Vehicle vehicle) throws ParkingLotException {
        if (isLotFull()) {
            throw new ParkingLotException("Lot is full");
        }
        if (isParked(vehicle)) {
            throw new ParkingLotException("Vehicle already parked");
        }
        parkedVehicles.add(vehicle);
        notifyIfFullLot();
    }

    boolean containVehicle(Vehicle vehicle) {
        return parkedVehicles.contains(vehicle);
    }

    private void notifyIfFullLot() {
        if (isLotFull()) {
            for (ParkingLotNotifiable lotNotifiable : parkingLotNotifiable) {
                lotNotifiable.notifyIsFull();
            }
        }
    }

    private void notifyIfSpaceIsAvailable() {
        for (ParkingLotNotifiable lotNotifiable : parkingLotNotifiable) {
            lotNotifiable.notifySpaceIsAvailable();
        }
    }

    void unPark(Vehicle vehicle) throws ParkingLotException {
        if (!isParked(vehicle)) {
            throw new ParkingLotException("Vehicle is not present");
        }
        parkedVehicles.remove(vehicle);
        notifyIfSpaceIsAvailable();
    }

    boolean isParked(Vehicle vehicle) {
        return parkedVehicles.contains(vehicle);
    }

    private boolean isLotFull() {
        return parkedVehicles.size() >= this.size;
    }

    private int freeSpace() {
        return this.size - parkedVehicles.size();
    }

    private int parkedVehicleSize() {
        return parkedVehicles.size();
    }

    ParkingLot compareSizeOfParkingLot(ParkingLot otherParkingLot) {
        if (this.freeSpace() > otherParkingLot.freeSpace()) {
            return this;
        }
        return otherParkingLot;
    }

    ParkingLot compareNumberOfVehiclesparked(ParkingLot otherParkingLot) {
        if (this.parkedVehicleSize() < otherParkingLot.parkedVehicleSize()) {
            return this;
        }
        return otherParkingLot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingLot that = (ParkingLot) o;
        return size == that.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size);
    }
}

