package swiggy.bootcamp;

class ParkingLotException extends Exception {
    ParkingLotException(String message) {
        super(message);
    }
}
