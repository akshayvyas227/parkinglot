package swiggy.bootcamp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@SuppressWarnings("unused")
public class Valet {
    private final ParkingLot fullParkingLot = new ParkingLot(0,new ArrayList<>(), new HashSet<>());
    private final List<ParkingLot> parkingLots;

    Valet(List<ParkingLot> parkingLots) {
        this(parkingLots, false);
    }

    Valet(List<ParkingLot> parkingLots, boolean isEfficient) {

        this.parkingLots = parkingLots;
        //this.champion = parkingLots.get(0);
    }


    ParkingLot whichParkingLot(Vehicle vehicle) throws ParkingLotException {
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.containVehicle(vehicle)) {
                return parkingLot;
            }
        }
        throw new ParkingLotException("Car is not parked");
    }

    void park(Vehicle vehicle, ParkingStyle parkingStyle) throws ParkingLotException {
        if(parkingLots.isEmpty()){
            throw new ParkingLotException("");
        }
        findCorrectLot(parkingLots, parkingStyle).park(vehicle);
    }

    private ParkingLot findCorrectLot(List<ParkingLot> availableLot, ParkingStyle parkingStyle) {
        if(parkingStyle == ParkingStyle.LOT_WITH_MOST_SPACES){
            return lotWithMostSpaces();
        }
        if(parkingStyle == ParkingStyle.LOT_WITH_LEAST_VEHICLES){
            return lotWithLeastParkables();
        }
        return  availableLot.get(0);
    }

    private ParkingLot lotWithMostSpaces() {
        ParkingLot champion = parkingLots.get(0);

        for (ParkingLot challenger : parkingLots) {
            champion = challenger.compareSizeOfParkingLot(champion);
        }
        return champion;
    }

    private ParkingLot lotWithLeastParkables() {
        ParkingLot champion = parkingLots.get(0);

        for (ParkingLot challenger : parkingLots) {
            champion = challenger.compareNumberOfVehiclesparked(champion);
        }
        return champion;
    }

    void unPark(Vehicle vehicle) throws ParkingLotException {
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isParked(vehicle)) {
                parkingLot.unPark(vehicle);
                return;
            }
        }
        throw new ParkingLotException("Car is not parked");
    }


}
